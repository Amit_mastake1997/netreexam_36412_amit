﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee
{
    class program
    {
        static void Main(string[] args)
        {

            Employee customer1 = new Employee()
            {
                EmpID = 1,
                EmpName = "Amit",
                EmpSalary = 50000
            };
            Employee customer2 = new Employee()
            {
                EmpID = 2,
                EmpName = "Rohan",
                EmpSalary = 60000
            };
            Employee customer3 = new Employee()
            {
                EmpID = 3,
                EmpName = "Pratik",
                EmpSalary = 55000
            };

            //using list  
            // Creating List with initial capacity 2  
            List<Employee> Customers = new List<Employee>(2);
            Customers.Add(customer1);
            Customers.Add(customer2);
            Customers.Add(customer3);

            //Accessing Item from the List using for loop  

            foreach (Employee c in Customers)
            {
                Console.WriteLine("ID={0}, Name={1}, Salary={2}", c.EmpID, c.EmpName, c.EmpSalary);
            }
        }

    }
    class Employee
    {
        public int EmpID { get; set; }
        public string EmpName { get; set; }
        public int EmpSalary { get; set; }
    }

}

